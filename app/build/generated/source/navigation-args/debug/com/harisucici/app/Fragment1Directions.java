package com.harisucici.app;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;

public class Fragment1Directions {
  private Fragment1Directions() {
  }

  @NonNull
  public static NavDirections actionFragment1ToFragment22() {
    return new ActionOnlyNavDirections(R.id.action_fragment1_to_fragment22);
  }
}
