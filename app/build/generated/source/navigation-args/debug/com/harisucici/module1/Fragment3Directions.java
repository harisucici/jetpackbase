package com.harisucici.module1;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.harisucici.app.R;

public class Fragment3Directions {
  private Fragment3Directions() {
  }

  @NonNull
  public static NavDirections actionFragment2ToFragment3() {
    return new ActionOnlyNavDirections(R.id.action_fragment2_to_fragment3);
  }
}
