// PayAidlInterface.aidl
package aidl;

// Declare any non-default types here with import statements

interface PayAidlInterface {
    int calculation(int anInt, int bnInt);
}