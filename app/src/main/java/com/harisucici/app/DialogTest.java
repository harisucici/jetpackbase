package com.harisucici.app;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.harisucici.app.databinding.DialogTestBinding;
import com.harisucici.jetpackbase.base.HBaseDialogFragment;
import com.harisucici.jetpackbase.base.NavigationActivity;


/**
 * Created by harisucici on 2020/5/27
 * Class description:  选择组织dialog
 */
public class DialogTest extends HBaseDialogFragment<DialogTestBinding, MainModel> {
    public static String TAG = "DIALOG_TEST";
    DialogTestBinding mBinding;


    public static DialogTest newInstance() {
        DialogTest fragment = new DialogTest();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected Boolean setFullScreen() {
        return false;
    }

    @Override
    protected int initDialogSite() {
        return Gravity.CENTER;
    }

    @Override
    public void initData(View view) {
        super.initData(view);

        Log.e(TAG, TAG);
        binding.text1.setOnClickListener(view12 -> mlistener.onDialogClick("text1", "111111"));
        binding.text2.setOnClickListener(view1 -> mlistener.onDialogClick("text2", "222222"));
    }

    @Override
    public int initContentView(LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        return R.layout.dialog_test;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }
}