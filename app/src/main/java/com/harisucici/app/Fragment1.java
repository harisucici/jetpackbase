package com.harisucici.app;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.navigation.Navigation;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.harisucici.app.databinding.Fragment1Binding;
import com.harisucici.jetpackbase.base.HBaseFragment;
import com.harisucici.jetpackbase.view.SnackbarUtil;

import org.jetbrains.annotations.NotNull;

public class Fragment1 extends HBaseFragment<Fragment1Binding, MainModel> {

    private String[] titles = {"Tab0", "Tab1", "Tab2"};
    private String title;

    @Override
    public int initContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return R.layout.fragment_1;
    }

    @Override
    public int initVariableId() {
        return BR.viewmodel;
    }


    @Override
    public void initViewObservable() {
        super.initViewObservable();
        viewModel.testDelayMsg.observeInFragment(this, s -> {
        });
    }

    @Override
    public void initParam() {
        super.initParam();
//        title=bundle.getString("dashabi");
//        viewModel.title.set(title);

        addDialog(DialogTest.TAG,new DialogTest());
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        showSnack("哈哈哈哈");
    }

    @Override
    public void onLaunchFourResult(ActivityResult result) {
        super.onLaunchFourResult(result);
        showSnack(result.getData().getStringExtra("MAP_VIEW_KEY"), SnackbarUtil.GREEN);
    }

//    @Override
//    public void initViewModelOwner() {
//        super.initViewModelOwner();
//        owner=this;
//    }
private class ViewPagerAdapter extends FragmentStateAdapter {

    public ViewPagerAdapter(@NonNull @NotNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    public ViewPagerAdapter(@NonNull @NotNull Fragment fragment) {
        super(fragment);
    }

    public ViewPagerAdapter(@NonNull @NotNull FragmentManager fragmentManager, @NonNull @NotNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @NotNull
    @Override
    public Fragment createFragment(int position) {
        if (position == 0) {
            return new Fragment2();
        } else if (position == 1) {
            return new Fragment2();
        } else if (position == 2) {
            return new Fragment2();
        } else {
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}

    @Override
    public void initData(View view) {
        super.initData(view);

//        binding.button.setOnClickListener(v -> viewModel.title.set("111111"));
        binding.button.setOnClickListener(v -> viewModel.showDialog(DialogTest.TAG));
        binding.button2.setOnClickListener(v -> {
            Navigation.findNavController(view).navigate(R.id.action_fragment1_to_fragment2);
        });
        
        ViewPagerAdapter adapter = new ViewPagerAdapter(this);
        binding.viewPager.setAdapter(adapter);
        new TabLayoutMediator(binding.tabLayout, binding.viewPager, true,new TabLayoutMediator.TabConfigurationStrategy(){
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText(titles[position]);
            }
        }).attach();
    }
}