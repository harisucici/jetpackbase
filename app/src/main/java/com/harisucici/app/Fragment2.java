package com.harisucici.app;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.harisucici.app.databinding.Fragment2Binding;
import com.harisucici.jetpackbase.base.HBaseFragment;


public class Fragment2 extends HBaseFragment<Fragment2Binding, MainModel> {


    private String title;
    private DialogTest dialog;

    @Override
    public int initContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return R.layout.fragment_2;
    }

    @Override
    public int initVariableId() {
        return BR.viewmodel;
    }
//    @Override
//    public void initViewModelOwner() {
//        super.initViewModelOwner();
//        owner=this;
//    }


    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        showSnack("哈哈哈哈");
    }

    @Override
    public void initParam() {
        super.initParam();
//        title = bundle.getString("dashabi");
//        viewModel.title.set(title);

    }

    @Override
    public void initData(View view) {
        super.initData(view);
        dialog = DialogTest.newInstance();
        dialog.setOnDialogListener((tag, s) -> {
            showSnack(tag + s);

        });
        addDialog(DialogTest.TAG, dialog);
        binding.button.setOnClickListener(v -> viewModel.title.set("222222"));
        binding.button2.setOnClickListener(v -> {
            showDialog(DialogTest.TAG);
//            Bundle bundle = new Bundle();
//            bundle.putString("dashabi", "大傻逼");
////            NavDirections directions = Fragment2Directions.actionFragment2ToModuleActivity("dashabi");
////            Navigation.findNavController(view).navigate(directions);
//            Navigation.findNavController(view).navigate(R.id.action_fragment2_to_moduleActivity, bundle);
        });

        viewModel.doRegisterReceiver();
    }


}