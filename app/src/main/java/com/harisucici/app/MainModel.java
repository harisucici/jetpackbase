package com.harisucici.app;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;

import com.harisucici.jetpackbase.base.HBaseViewModel;
import com.harisucici.jetpackbase.base.UnLastingLiveData;
import com.harisucici.jetpackbase.broadcast.MessageReceiver;
import com.harisucici.jetpackbase.view.SnackbarUtil;
import com.harisucici.skywarp.SkywarpObserver;

import kotlin.jvm.internal.Reflection;

public class MainModel extends HBaseViewModel {
    public ObservableField<String> title = new ObservableField<>("hello");
    private MessageReceiver receiver;

    public ObservableField<String> text = new ObservableField<>("fuck xxx");
    public MainModel(@NonNull Application application) {
        super(application);
    }

    public UnLastingLiveData<String> testDelayMsg = new UnLastingLiveData<>();

    public void shabi() {
//         UnLastingLiveData<Boolean> toAddSlideListener = new UnLastingLiveData.Builder<Boolean>().setAllowNullValue(false).create();
//        Bundle bundle = new Bundle();
//        bundle.putString("dashabi", "dashabi11111");
//        startContainerActivity(Fragment2.class.getCanonicalName(),bundl~`e);
//        startNavActivity(R.navigation.nav_test2,bundle);

//        startActivity(SecondActivity.class);

//        showDialog("jajajajajal");
//        showShortSnack("hgiahahah", SnackbarUtil.ALERT);
        dismissDialog(DialogTest.TAG);


//        startActivity(Reflection.getOrCreateKotlinClass(ThirdActivity.class).getClass());

//        showDialog(DialogTest.TAG);

    }

    public void shabi2() {

        showDialog("jajajajajal");
        showShortSnack("hahahah", SnackbarUtil.ALERT);
    }

    @Override
    protected void onMsgReceive(int code, String msg) {
        super.onMsgReceive(code, msg);
        showShortSnack(msg);
    }

    @Override
    public void onStateChanged(@NonNull LifecycleOwner source, @NonNull Lifecycle.Event event) {
        super.onStateChanged(source, event);
        if(event== Lifecycle.Event.ON_DESTROY){
            SkywarpObserver.getInstance().stopObserve();
        }
    }
}
