package com.harisucici.app;


import android.os.Bundle;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import com.harisucici.app.databinding.ActivitySecondBinding;
import com.harisucici.jetpackbase.base.HBaseActivity;

public class SecondActivity extends HBaseActivity<ActivitySecondBinding, MainModel> {


    @Override
    public int initContentView(Bundle savedInstanceState) {
        return R.layout.activity_second;
    }

    @Override
    public int initVariableId() {
        return BR.viewmodel;
    }

    @Override
    public void initData(View v) {
        super.initData(v);
//        Intent intent = new Intent();
//        intent.putExtra("MAP_VIEW_KEY","hahahahaah");
//        setResult(999, intent);
//        onBackPressed();
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host);
        NavController navController = navHostFragment.getNavController();
        NavUI.setupMyWithNavController(binding.navBottom, navController);
    }

    @Override
    public boolean onSupportNavigateUp() {
        return Navigation.findNavController(this, R.id.nav_bottom).navigateUp();
    }
}