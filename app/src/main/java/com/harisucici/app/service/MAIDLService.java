package com.harisucici.app.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.harisucici.jetpackbase.broadcast.MessageReceiver;

import aidl.PayAidlInterface;


public class MAIDLService extends Service {
    PayAidlInterface.Stub mBinder = new PayAidlInterface.Stub() {
        @Override
        public int calculation(int anInt, int bnInt) throws RemoteException {
            Log(anInt + "--" + bnInt);
            Intent intent = new Intent();
            intent.setAction(MessageReceiver.ACTION);
            intent.putExtra(MessageReceiver.CODE, 666);
            intent.putExtra(MessageReceiver.MSG, anInt + "--" + bnInt);
            sendBroadcast(intent);
            return 8888;
        }
    };

    private void Log(String str) {
        Log.e("MAIDLService", "----------" + str + "----------");
    }

    @Override
    public void onCreate() {
        Log("service created");
    }

    @Override
    public IBinder onBind(Intent t) {
        Log("service on bind");
        return mBinder;
    }

    @Override
    public void onDestroy() {
        Log("service on destroy");
        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log("service on unbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        Log("service on rebind");
        super.onRebind(intent);
    }
}