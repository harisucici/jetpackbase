package com.harisucici.jetpackbase.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavArgument;
import androidx.navigation.NavBackStackEntry;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.harisucici.jetpackbase.base.HBaseViewModel.ParameterField;
import com.harisucici.jetpackbase.view.SnackbarUtil;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;


public abstract class HBaseFragment<V extends ViewDataBinding, VM extends HBaseViewModel> extends Fragment implements IBaseView {
    protected V binding;
    protected VM viewModel;
    protected Bundle bundle;
    protected ViewModelStoreOwner owner = null;
    protected Map<String,DialogFragment> dialogMap = new HashMap<>(5);
    protected NavController controller;
    private int viewModelId;
    private NavArgument navArgument;
    private DialogFragment dialog;
    private ActivityResultLauncher<Intent> launchForResult;
    private OnBackPressedDispatcher dispatcher;
    private Fragment parentFragment;
    private Context context;
    private OnBackPressedCallback callback = new OnBackPressedCallback(true) {
        @Override
        public void handleOnBackPressed() {
            onBackPressed();
        }
    };

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        // 自带一组 launch
        launchForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            if (result.getData() == null) {
                return;
            }
            onLaunchFourResult(result);
        });
        initLaunchForResult();
    }

    protected void initLaunchForResult() {

    }

    public void onLaunchFourResult(ActivityResult result) {


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dispatcher = requireActivity().getOnBackPressedDispatcher();
        dispatcher.addCallback(this, callback);
        initViewModelOwner();
        createViewModel();

        if (owner instanceof NavBackStackEntry) {
            Map<String, NavArgument> map = controller.getGraph().getArguments();
            if (map.size() != 0) {
                navArgument = map.get("BUNDLE");
                bundle = (Bundle) navArgument.getDefaultValue();
            }
        } else if (getParentFragment() instanceof NavHostFragment) {
            Map<String, NavArgument> map = NavHostFragment.findNavController(this).getGraph().getArguments();
            if (map.size() != 0) {
                navArgument = map.get("BUNDLE");
                bundle = (Bundle) navArgument.getDefaultValue();
            }
        } else {
            bundle = getArguments();
        }

        initParam();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // dataBinding
        binding = DataBindingUtil.inflate(inflater, initContentView(inflater, container, savedInstanceState), container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (binding != null) {
            binding.unbind();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewObservable();
        initViewDataBinding();
        registorUIChangeLiveDataCallBack();
        initData(view);

    }

    private void createViewModel() {
        viewModelId = initVariableId();
        viewModel = initViewModel();
        if (viewModel == null) {
            Class modelClass;
            Type type = getClass().getGenericSuperclass();
            if (type instanceof ParameterizedType) {
                modelClass = (Class) ((ParameterizedType) type).getActualTypeArguments()[1];
            } else {
                //如果没有指定泛型参数，则默认使用BaseViewModel
                modelClass = HBaseViewModel.class;
            }
            viewModel = (VM) createViewModel(owner, modelClass);
        }
    }

    /**
     * 注入绑定
     */
    private void initViewDataBinding() {

        binding.setVariable(viewModelId, viewModel);
        //支持LiveData绑定xml，数据改变，UI自动会更新
        binding.setLifecycleOwner(this);
        //让ViewModel拥有View的生命周期感应
        getLifecycle().addObserver(viewModel);
    }

    /**
     * =====================================================================
     **/
    //注册ViewModel与View的契约UI回调事件
    protected void registorUIChangeLiveDataCallBack() {
        //加载对话框显示
        viewModel.getUC().getShowDialogEvent().observe(this, tag -> {
            showDialog(tag);
        });
        //加载对话框消失
        viewModel.getUC().getDismissDialogEvent().observe(this, tag -> {
            dismissDialog(tag);
        });
        //snackbar 显示
        viewModel.getUC().getShowSnackEvent().observe(this, message -> {
            showSnack(message);
        });
        viewModel.getUC().getShowSnackTypeEvent().observe(this, message -> {
            String[] msg = message.split("&&");
            showSnack(msg[0], Integer.parseInt(msg[1]));
        });
        //跳入新页面
        viewModel.getUC().getStartActivityEvent().observe(this, params -> {
            Class<?> clz = (Class<?>) params.get(ParameterField.CLASS);
            Bundle bundle = (Bundle) params.get(ParameterField.BUNDLE);
            startActivity(clz, bundle);
        });
        //跳入ContainerActivity
        viewModel.getUC().getStartContainerActivityEvent().observe(this, params -> {
            String canonicalName = (String) params.get(ParameterField.CANONICAL_NAME);
            Bundle bundle = (Bundle) params.get(ParameterField.BUNDLE);
            startContainerActivity(canonicalName, bundle);
        });
        //跳入NavActivity
        viewModel.getUC().getStartNavActivityEvent().observe(this, params -> {
            int navigationId = (Integer) params.get(ParameterField.NAVIGATION_ID);
            Bundle bundle = (Bundle) params.get(ParameterField.BUNDLE);
            startNavActivity(navigationId, bundle);
        });
        //Activity4Result
        viewModel.getUC().getStartActivity4ResultEvent().observe(this, params -> {
            Class<?> clz = (Class<?>) params.get(ParameterField.CLASS);
            Bundle bundle = (Bundle) params.get(ParameterField.BUNDLE);
            startActivity4Result(clz, bundle);
        });
        //关闭界面
        viewModel.getUC().getFinishEvent().observe(this, v -> requireActivity().finish());
        //关闭上一层
        viewModel.getUC().getOnBackPressedEvent().observe(this, v -> onBackPressed());
    }

    /**
     * 跳转页面
     *
     * @param clz 所跳转的目的Activity类
     */
    public void startActivity(Class<?> clz) {
        startActivity(new Intent(getContext(), clz));
    }

    /**
     * 跳转页面
     *
     * @param clz    所跳转的目的Activity类
     * @param bundle 跳转所携带的信息
     */
    public void startActivity(Class<?> clz, Bundle bundle) {
        Intent intent = new Intent(getContext(), clz);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    /**
     * 跳转页面带回数据
     *
     * @param intent 所跳转的intent
     */
    public void startActivity4Result(Intent intent) {
        launchForResult.launch(intent);
    }

    /**
     * 跳转页面带回数据
     *
     * @param intent 所跳转的intent
     */
    public void startActivity4Result(ActivityResultLauncher<Intent> launch, Intent intent) {
        launch.launch(intent);
    }

    /**
     * 跳转页面带回数据
     *
     * @param clz 所跳转的目的Activity类
     */
    public void startActivity4Result(Class<?> clz) {
        launchForResult.launch(new Intent(getContext(), clz));
    }

    /**
     * 跳转页面带回数据
     *
     * @param clz    所跳转的目的Activity类
     * @param bundle 跳转所携带的信息
     */
    public void startActivity4Result(Class<?> clz, Bundle bundle) {
        Intent intent = new Intent(getContext(), clz);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        launchForResult.launch(intent);
    }

    /**
     * 跳转容器页面
     *
     * @param canonicalName 规范名 : Fragment.class.getCanonicalName()
     */
    public void startContainerActivity(String canonicalName) {
        startContainerActivity(canonicalName, null);
    }

    /**
     * 跳转容器页面
     *
     * @param canonicalName 规范名 : Fragment.class.getCanonicalName()
     * @param bundle        跳转所携带的信息
     */
    public void startContainerActivity(String canonicalName, Bundle bundle) {
        Intent intent = new Intent(getContext(), ContainerActivity.class);
        intent.putExtra(ContainerActivity.FRAGMENT, canonicalName);
        if (bundle != null) {
            intent.putExtra(ContainerActivity.BUNDLE, bundle);
        }
        startActivity(intent);
    }

    /**
     * 跳转Navigation页面
     *
     * @param navId 规范名 : R.navigation.navId
     */
    public void startNavActivity(int navId) {
        Intent intent = new Intent(getContext(), NavigationActivity.class);
        intent.putExtra(NavigationActivity.NAVID, navId);
        startActivity(intent);
    }

    /**
     * 跳转Navigation页面
     *
     * @param navId  规范名 : R.navigation.navId
     * @param bundle 跳转所携带的信息
     */
    public void startNavActivity(int navId, Bundle bundle) {
        Intent intent = new Intent(getContext(), NavigationActivity.class);
        intent.putExtra(NavigationActivity.NAVID, navId);
        if (bundle != null) {
            intent.putExtra(NavigationActivity.BUNDLE, bundle);
        }
        startActivity(intent);
    }

    /**
     * =====================================================================
     **/

    //刷新布局
    public void refreshLayout() {
        if (viewModel != null) {
            binding.setVariable(viewModelId, viewModel);
        }
    }

    @Override
    public void initParam() {

    }

    /**
     * 初始化根布局
     *
     * @return 布局layout的id
     */
    public abstract int initContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);

    /**
     * 初始化ViewModel的id
     *
     * @return BR的id
     */
    public abstract int initVariableId();

    /**
     * 初始化ViewModel
     *
     * @return 继承BaseViewModel的ViewModel
     */
    public VM initViewModel() {
        return null;
    }

    @Override
    public void initData(View view) {

    }

    @Override
    public void initViewModelOwner() {
        if (owner == null) {
            owner = requireActivity();
        }
        context = requireContext();
        if (context instanceof NavigationActivity) {
            parentFragment = requireParentFragment();
            //如果是 Navigation
//            if (parentFragment instanceof NavHostFragment) {
            controller = NavHostFragment.findNavController(this);
            owner = controller.getBackStackEntry(controller.getGraph().getId());
//            }
        }

    }

    @Override
    public void initViewObservable() {

    }

    @SuppressLint("RestrictedApi")
    protected void onBackPressed() {
        if (context instanceof NavigationActivity &&
                controller.getBackStack().size() > 2) {
            controller.navigateUp();
        } else {
            requireActivity().finish();
        }

    }


    /**
     * 创建ViewModel
     *
     * @param cls
     * @param <T>
     * @return
     */
    public <T extends ViewModel> T createViewModel(ViewModelStoreOwner owner, Class<T> cls) {
        return new ViewModelProvider(owner, new ViewModelProvider.AndroidViewModelFactory(requireActivity().getApplication())).get(cls);
    }


    public void showDialog(String tag) {
        dialog = dialogMap.get(tag);
        if (dialog != null) {
            dialog.show(getChildFragmentManager(), tag);
        }

    }

    public void dismissDialog(String tag) {
        dialog = dialogMap.get(tag);
        if (dialog != null) {
            dialog.dismiss();
        }
        dialog = null;
    }

    public void addDialog(String tag, DialogFragment dialog) {
        dialogMap.put(tag, dialog);
    }

    public void removeDialog(DialogFragment dialog) {
        this.dialog = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            dialogMap.remove(dialog.getTag(), dialog);
        } else {
            dialogMap.remove(dialog);
        }
    }


    public void showSnack(@NonNull String message) {
        InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(requireActivity().getWindow().getDecorView().getWindowToken(), 0);
        }
        SnackbarUtil.showBarShortTime(requireActivity().findViewById(android.R.id.content), message, SnackbarUtil.INFO);
    }

    public void showSnack(@NonNull String message, int type) {
        InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(requireActivity().getWindow().getDecorView().getWindowToken(), 0);
        }
        SnackbarUtil.showBarShortTime(requireActivity().findViewById(android.R.id.content), message, type);
    }

}
