package com.harisucici.jetpackbase.base;

import android.app.Application;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleOwner;

import com.harisucici.jetpackbase.broadcast.MessageReceiver;
import com.harisucici.jetpackbase.bus.UIChangeLiveData;

import java.util.HashMap;
import java.util.Map;


public class HBaseViewModel extends AndroidViewModel implements LifecycleEventObserver {
    public Application application;
    private UIChangeLiveData uc;
    private String SNACK_FORMAT = "%s&&%s";
    private MessageReceiver receiver;

    public HBaseViewModel(@NonNull Application application) {
        super(application);
        this.application = application;

    }

    public UIChangeLiveData getUC() {
        if (uc == null) {
            uc = new UIChangeLiveData();
        }
        return uc;
    }

    public void showDialog(String tag) {
        uc.showDialogEvent.postValue(tag);
    }

    public void dismissDialog(String tag) {
        uc.dismissDialogEvent.postValue(tag);
    }

    public void showShortSnack(String message) {
        uc.showSnackEvent.postValue(message);
    }

    public void showShortSnack(String message, int type) {
        uc.showSnackTypeEvent.postValue(String.format(SNACK_FORMAT, message, type));
    }


    /**
     * 跳转页面
     *
     * @param clz 所跳转的目的Activity类
     */
    public void startActivity(Class<?> clz) {
        startActivity(clz, null);
    }

    /**
     * 跳转页面
     *
     * @param clz    所跳转的目的Activity类
     * @param bundle 跳转所携带的信息
     */
    public void startActivity(Class<?> clz, Bundle bundle) {
        Map<String, Object> params = new HashMap<>(2);
        params.put(ParameterField.CLASS, clz);
        if (bundle != null) {
            params.put(ParameterField.BUNDLE, bundle);
        }
        uc.startActivityEvent.postValue(params);
    }

    /**
     * 跳转页面带回数据
     *
     * @param clz 所跳转的目的Activity类
     */
    public void startActivity4Result(Class<?> clz) {
        startActivity4Result(clz, null);
    }

    /**
     * 跳转页面带回数据
     *
     * @param clz    所跳转的目的Activity类
     * @param bundle 跳转所携带的信息
     */
    public void startActivity4Result(Class<?> clz, Bundle bundle) {
        Map<String, Object> params = new HashMap<>(5);
        params.put(ParameterField.CLASS, clz);
        if (bundle != null) {
            params.put(ParameterField.BUNDLE, bundle);
        }
        uc.startActivity4ResultEvent.postValue(params);
    }

    /**
     * 跳转容器页面
     *
     * @param canonicalName 规范名 : Fragment.class.getCanonicalName()
     */
    public void startContainerActivity(String canonicalName) {
        startContainerActivity(canonicalName, null);
    }

    /**
     * 跳转容器页面
     *
     * @param canonicalName 规范名 : Fragment.class.getCanonicalName()
     * @param bundle        跳转所携带的信息
     */
    public void startContainerActivity(String canonicalName, Bundle bundle) {
        Map<String, Object> params = new HashMap<>(2);
        params.put(ParameterField.CANONICAL_NAME, canonicalName);
        if (bundle != null) {
            params.put(ParameterField.BUNDLE, bundle);
        }
        uc.startContainerActivityEvent.postValue(params);
    }

    /**
     * 跳转Navigation页面
     *
     * @param navId 规范名 : R.navigation.navId
     */
    public void startNavActivity(int navId) {
        startNavActivity(navId, null);
    }


    /**
     * 跳转Navigation页面
     *
     * @param navId  规范名 : R.navigation.navId
     * @param bundle 跳转所携带的信息
     */
    public void startNavActivity(int navId, Bundle bundle) {
        Map<String, Object> params = new HashMap<>(2);
        params.put(ParameterField.NAVIGATION_ID, navId);
        if (bundle != null) {
            params.put(ParameterField.BUNDLE, bundle);
        }
        uc.startNavActivityEvent.postValue(params);
    }

    /**
     * 关闭界面
     */
    public void finish() {
        uc.finishEvent.call();
    }

    /**
     * 返回上一层
     */
    public void onBackPressed() {
        uc.onBackPressedEvent.call();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (receiver != null) {
            application.unregisterReceiver(receiver);
        }

    }

    public void doRegisterReceiver() {
        receiver = new MessageReceiver() {
            @Override
            protected void receive(int code, String msg) {
                Log.e("MAIDLService", code + msg);
                onMsgReceive(code, msg);
            }
        };
        IntentFilter filter = new IntentFilter(MessageReceiver.ACTION);
        application.registerReceiver(receiver, filter);
    }

    // 广播接收器
    protected void onMsgReceive(int code, String msg) {

    }

    @Override
    public void onStateChanged(@NonNull LifecycleOwner source, @NonNull Lifecycle.Event event) {

    }

    public static final class ParameterField {
        public static String CLASS = "CLASS";
        public static String CANONICAL_NAME = "CANONICAL_NAME";
        public static String NAVIGATION_ID = "NAVIGATION_ID";
        public static String BUNDLE = "BUNDLE";
    }


}
