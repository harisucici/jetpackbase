package com.harisucici.jetpackbase.base;


import android.view.View;

public interface IBaseView {
    /**
     * 初始化界面传递参数
     */
    void initParam();

    /**
     * 初始化数据
     */
    void initData(View v);

    /**
     * 初始化界面观察者的监听
     */
    void initViewObservable();

    /**
     * 设置ViewModel提供者
     */
    void initViewModelOwner();
}
