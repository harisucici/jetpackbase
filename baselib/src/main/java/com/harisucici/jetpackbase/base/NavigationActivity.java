package com.harisucici.jetpackbase.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavArgument;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.fragment.NavHostFragment;

import com.harisucici.jetpackbase.R;

public class NavigationActivity extends AppCompatActivity {

    public final static String NAVID = "NAVID";
    public static final String BUNDLE = "BUNDLE";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host);
        NavController controller = navHostFragment.getNavController();
        controller.setGraph(getIntent().getIntExtra(NAVID, 0));
        NavGraph graph = controller.getGraph();
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle == null) {
            return;
        }
        NavArgument argument = new NavArgument.Builder().setDefaultValue(bundle).build();
        graph.addArgument(BUNDLE, argument);
    }
}