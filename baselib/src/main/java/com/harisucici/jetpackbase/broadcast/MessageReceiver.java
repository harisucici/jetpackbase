package com.harisucici.jetpackbase.broadcast;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public abstract class MessageReceiver extends BroadcastReceiver {
    public final static String ACTION = "com.harisucici.jetpackbase.broadcast.MessageReceiver";
    public final static String MSG = "MSG";
    public final static String CODE = "CODE";

    @Override
    public void onReceive(Context context, Intent intent) {
        int code = intent.getIntExtra(CODE, -1);
        String msg = intent.getStringExtra(MSG);
        receive(code, msg);
    }

    protected abstract void receive(int code, String msg);
}
