package com.harisucici.jetpackbase.bus;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import java.util.Map;

public class UIChangeLiveData extends SingleLiveEvent {
    public SingleLiveEvent<String> showDialogEvent;
    public SingleLiveEvent<String> showSnackEvent;
    public SingleLiveEvent<String> showSnackTypeEvent;
    public SingleLiveEvent<String> dismissDialogEvent;
    public SingleLiveEvent<Map<String, Object>> startActivityEvent;
    public SingleLiveEvent<Map<String, Object>> startContainerActivityEvent;
    public SingleLiveEvent<Map<String, Object>> startNavActivityEvent;

    public SingleLiveEvent<Map<String, Object>> startActivity4ResultEvent;
    public SingleLiveEvent<Void> finishEvent;
    public SingleLiveEvent<Void> onBackPressedEvent;

    public SingleLiveEvent<String> getShowDialogEvent() {
        return showDialogEvent = createLiveData(showDialogEvent);
    }

    public SingleLiveEvent<String> getShowSnackEvent() {
        return showSnackEvent = createLiveData(showSnackEvent);
    }

    public SingleLiveEvent<String> getShowSnackTypeEvent() {
        return showSnackTypeEvent = createLiveData(showSnackTypeEvent);
    }

    public SingleLiveEvent<String> getDismissDialogEvent() {
        return dismissDialogEvent = createLiveData(dismissDialogEvent);
    }

    public SingleLiveEvent<Map<String, Object>> getStartActivityEvent() {
        return startActivityEvent = createLiveData(startActivityEvent);
    }

    public SingleLiveEvent<Map<String, Object>> getStartContainerActivityEvent() {
        return startContainerActivityEvent = createLiveData(startContainerActivityEvent);
    }

    public SingleLiveEvent<Map<String, Object>> getStartNavActivityEvent() {
        return startNavActivityEvent = createLiveData(startNavActivityEvent);
    }

    public SingleLiveEvent<Map<String, Object>> getStartActivity4ResultEvent() {
        return startActivity4ResultEvent = createLiveData(startActivity4ResultEvent);
    }

    public SingleLiveEvent<Void> getFinishEvent() {
        return finishEvent = createLiveData(finishEvent);
    }

    public SingleLiveEvent<Void> getOnBackPressedEvent() {
        return onBackPressedEvent = createLiveData(onBackPressedEvent);
    }

    private <T> SingleLiveEvent<T> createLiveData(SingleLiveEvent<T> liveData) {
        if (liveData == null) {
            liveData = new SingleLiveEvent<>();
        }
        return liveData;
    }

    @Override
    public void observe(LifecycleOwner owner, Observer observer) {
        super.observe(owner, observer);
    }
}
