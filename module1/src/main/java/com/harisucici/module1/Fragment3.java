package com.harisucici.module1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.harisucici.jetpackbase.base.HBaseFragment;
import com.harisucici.module1.databinding.Fragment3Binding;

public class Fragment3 extends HBaseFragment<Fragment3Binding, ModuleModel> {
    @Override
    public int initContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return R.layout.fragment_3;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }
}
