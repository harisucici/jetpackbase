package com.harisucici.module1;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;

import com.harisucici.jetpackbase.base.HBaseActivity;
import com.harisucici.module1.databinding.ActivityModuleBinding;

import aidl.PayAidlInterface;

public class ModuleActivity extends HBaseActivity<ActivityModuleBinding, ModuleModel> {
    private PayAidlInterface service;
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Log.e("MAIDLService", "onServiceDisconnected:" + arg0.getPackageName());
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            Log.e("MAIDLService", "onServiceConnected:" + name.getPackageName());
            // 获取远程Service的onBinder方法返回的对象代理
            service = PayAidlInterface.Stub.asInterface(binder);
        }
    };

    @Override
    public void initParam() {
        super.initParam();
        String value = getPackageName();

    }

    @Override
    public void initData(View v) {
        super.initData(v);
        Bundle bundle = getIntent().getExtras();
        Log.e("dashabi", bundle.getString("dashabi"));
        //使用意图对象绑定开启服务
        Intent intent = new Intent();
        //在5.0及以上版本必须要加上这个
        intent.setPackage(getPackageName());
        intent.setAction("com.harisucici.app.service.MAIDLService");
        bindService(intent, mServiceConnection, BIND_AUTO_CREATE);

        binding.textView.setOnClickListener(view -> {
            //调用
            if (service != null) {
                try {
                    int calculation = service.calculation(9999, 9999);
                    binding.textView.setText("calculation:" + calculation);
                    if (calculation == 8888) {
//                        onBackPressed();

                        startContainerActivity(Fragment3.class.getCanonicalName());
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mServiceConnection != null) {
            unbindService(mServiceConnection);
        }
    }

    @Override
    public int initContentView(Bundle savedInstanceState) {
        return R.layout.activity_module;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }
}