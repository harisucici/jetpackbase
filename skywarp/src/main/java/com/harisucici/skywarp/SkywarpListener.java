package com.harisucici.skywarp;

import java.io.File;

public interface SkywarpListener {
    void editFile(File file);
}
